package io.suprnation

import org.scalatest.funsuite.AnyFunSuite
import io.suprnation.Main._
import io.suprnation.Utils._

class MainTest extends AnyFunSuite {

  test("can pass parameters to the app") {
    main( Array("test") )
    assert(true)
  }

  test( "can create a Leaf") {
    val leaf = Leaf(1)
    assert(leaf.value == 1)
  }

  test("can create a Node") {
    val node = Node(1, Leaf(2), Leaf(3))
    assert(node.v == 1)
    assert(node.l == Leaf(2))
    assert(node.r == Leaf(3))
  }

  test("can convert array of String in array of arrays of int") {
    val source = scala.io.Source.fromResource("triangle1.txt").getLines().toArray
    val arrayOfInt : Array[Array[Int]] = split(source)
    assert(arrayOfInt(0)(0) == 1)
    assert(arrayOfInt(1)(0) == 2)
    assert(arrayOfInt(1)(1) == 3)
    assert(arrayOfInt(2)(0) == 4)
    assert(arrayOfInt(2)(1) == 5)
    assert(arrayOfInt(2)(2) == 6)
  }

  test("can create a a Leaf from basic split structure") {
    // given I have a structure representing a single leaf
    val myStructure = Array(Array(1))

    //when I can get a tree
    val myTree: Tree = asTree(myStructure)

    //then my Tree is correct
    assert(myTree.isInstanceOf[Leaf])
    assert(myTree.value == 1)
    assert(myTree.left == null)
    assert(myTree.right == null)
  }

  test("can create a tree from a simple split structure") {
    // given I have a structure representing a tree
    val myStructure : Triangle = Array(Array(1), Array(2, 3))

    //when I can get a tree
    val myTree: Tree = myStructure.toTree

    //then my Tree is correct
    assert(myTree.value == 1)
    assert(myTree.left.value == 2)
    assert(myTree.right.value == 3)
  }

    test("can create a tree from the split structure") {
    // given I have a structure representing a tree
    val myStructure = Array(Array(1), Array(2, 3), Array(4, 5, 6))

    //when I can get a tree
    val myTree: Tree = myStructure.toTree

    //then my Tree is correct
    assert(myTree.value == 1)
    val left = myTree.left
    val right = myTree.right
    assert(left.value == 2)
    assert(right.value == 3)
    assert(left.left.value == 4)
    assert(left.right.value == 5)
    assert(right.left.value == 5)
    assert(right.right.value == 6)
  }

  test("can find minimal path in a simple example") {
    val myTree = Node(1,
          Node( 2, Leaf(4), Leaf(5) ),
          Node( 3, Leaf(5), Leaf(6) )
    )
    assert(myTree.minimalPath == 7)
  }

  test("can find minimal path of the assignment") {
    assert(split(scala.io.Source.fromResource("assignment.txt").getLines().toArray).toTree.minimalPath == 18)
  }

  test("no out of memory error please") {
    val tree = split(scala.io.Source.fromResource("bigTriangle.txt").getLines().toArray).toTree
    println("tree created")
    tree.minimalPath
  }

}
