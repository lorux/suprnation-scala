package io.suprnation

import io.suprnation.Utils._

import scala.annotation.tailrec

object Main extends App {

  def split(source: Array[String]) : Triangle = {
    split(source, Array[Array[Int]]())
  }

  @tailrec
  def split(source: Array[String], out: Triangle): Triangle = {
    if (source.isEmpty) out
    else split(source.tail, out :+ source.head.split(" ").map(s => s.toInt))
  }

  if (args.isEmpty) {
    var input = scala.collection.mutable.ArrayBuffer.empty[String]
    var line = scala.io.StdIn.readLine()
    var i = 0
    while( line != null) {
      input += line
      i = i + 1
      line = scala.io.StdIn.readLine()
    }
    println ("Minimal path is: " + split(input.toArray).toTree.minimalPath)
  }
}

trait Tree {
  val left : Tree
  val right : Tree
  val value : Int
  val minimalPath: Int

}

case class Leaf(v : Int) extends Tree {
  override val left = null
  override val right = null
  override val value: Int = v
  override val minimalPath: Int = value
}

case class Node(v : Int, l: Tree, r: Tree) extends Tree {
  override val left = l
  override val right = r
  override val value: Int = v
  override val minimalPath: Int = left.minimalPath.min(right.minimalPath) + value
}

package object Utils {
  type Triangle = Array[Array[Int]]

  implicit class Structure(t: Triangle) {
    def toTree : Tree = asTree(t)
  }

  def asTree(myStructure: Triangle) : Tree = {
    val reversed = myStructure.reverse
    var previousLine : Array[Tree] = reversed.head.map(value => Leaf(value))
    for (line <- reversed.tail) {
      previousLine = line.zipWithIndex.map(value => Node(value._1, previousLine(value._2), previousLine(value._2 + 1)))
    }
    previousLine.head
  }

}
