I give for granted that the triangle is well formed.

A pre-check would be easy to implement (each row is 1 element longer than the previous, and all the elements are positive numbers)